import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Header, CardSection, Card } from './components/common';
import Time from './components/Time'

const App = () => {
    return (
        <View style={{ flex: 1 }}>
            <Header headerText="TEST MOBILE APP" />
            <Time />
        </View>
    )
}
export default App