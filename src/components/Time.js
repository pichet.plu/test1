import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Header, CardSection, TextTime, Colon } from './common';

import moment from 'moment';
class time extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time_hour: moment().format("h"),
            time_minutes: moment().format("mm")
        };
    }
    componentDidMount() {
        this.intervalTime = setInterval(
            () => this.getTime(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.intervalTime);

    }

    getTime() {
        this.setState({
            time_hour: moment().format("h"),
            time_minutes: moment().format("mm")
        })
    }
    render() {
        const { time_hour, time_minutes } = this.state;
        return (
            <View style={styles.container}>
                <CardSection>
                    <TextTime typeText={"Hour"} timeText={time_hour} />
                    <Colon />
                    <TextTime typeText={"Minutes"} timeText={time_minutes} />
                </CardSection>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

});


export default time