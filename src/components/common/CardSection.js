import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    flexDirection: 'row',
    flex:1,
    width:'80%',
    height:'40%',
    borderRadius: 50,
    borderWidth: 5,
    borderColor: '#000',
    backgroundColor:'#008080'
  }
};

export { CardSection };
