import React from 'react';
import { Text, View } from 'react-native';

const Colon = (props) => {
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>
                :
            </Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        alignItems: 'center', 
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 100
    }
};
export { Colon };
