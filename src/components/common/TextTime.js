import React from 'react';
import { Text, View } from 'react-native';

const TextTime = (props) => {
  const { textStyle, textHeadStyle, viewStyle, textTimeStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textHeadStyle}>{props.typeText}</Text>
      <View style={textTimeStyle}>
        <Text style={textStyle}>
          {props.timeText}
        </Text>
      </View>
    </View>
  );
};

const styles = {
  viewStyle: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
    position: 'relative'
  },
  textStyle: {
    fontSize: 85,
   fontFamily: 'M0N0T3CHN0L0GY2407'
  },
  textHeadStyle: {
    fontSize: 35,
    color: '#fff',
   // fontFamily: 'M0N0T3CHN0L0GY2407'
  },
  textTimeStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    height: '60%',
    backgroundColor: '#fff',
    borderRadius: 30,
    borderWidth: 5,
    borderColor: '#000',
  }
};

export { TextTime };
